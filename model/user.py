import sqlite3
from miUtil import *


def CreateTable():
 try:
    conn = sqlite3.connect('database.db')
    print ("Opened database successfully");
    conn.execute('CREATE TABLE IF NOT EXISTS "user" (Id INTEGER  PRIMARY KEY AUTOINCREMENT, fistName TEXT, lastName TEXT, email TEXT,password TEXT,isActive INTEGER, key TEXT)')
    print ("Table created successfully");
    return toJSON({'status':'Ok'})
 except Exception as e:
     return toJSON({'status':'Fail','error':PrintException()})
class User():
    def __init__(self):
        self.email = None
        self.password = None
        self.fistName=None
        self.lastName=None
        self.password = None
        self.isActive = None
        self.key = None
    def CreateUser(self,fistName,lastName,email,password,isActive,key):
         try:

             self.email = email
             self.password = password
             self.fistName = fistName
             self.lastName = lastName
             self.password = password
             self.isActive = isActive
             self.key = key
             conn = sqlite3.connect('database.db')

             #query='insert into user (fistName, lastName, email ,password ,isActive , key) values('+str(fistName)+','+str(lastName)+','+str(email)+','+str(password)+','+str(isActive)+','+str(key)+')'
             query='insert into user (fistName, lastName, email ,password ,isActive , key) values(?,?,?,?,?,?)'

             print('query :')
             print(query)
             conn.execute(query,(fistName,lastName,email,password,isActive,key))
             conn.commit()
             print("data isert with successfully");
             conn.close()
             print('user created')
         except Exception as e:
             print(PrintException())
    def get_user_by_key(self,key):
        try:
            conn = sqlite3.connect('database.db')
            curssor = conn.cursor()
            query="""select * from user where key = ?"""
            curssor.execute(query,(key,))
            records = curssor.fetchall()
            res={}
            for i in records:
                res={'fistName':i[1],'lastName':i[2],'email':i[3],'password':i[4],'isActive':i[5],'key':i[6]}
            conn.close()
            return res
        except Exception as e:
            print(PrintException())

    def get_user_by_email(self, email):
        try:
            conn = sqlite3.connect('database.db')
            curssor = conn.cursor()
            query = """select * from user where email = ?"""
            curssor.execute(query, (email,))
            records = curssor.fetchall()
            res = {}
            for i in records:
                res = {'fistName': i[1], 'lastName': i[2], 'email': i[3], 'password': i[4], 'isActive': i[5],
                       'key': i[6]}
            conn.close()
            return res
        except Exception as e:
            print(PrintException())
    def find_user_email(self,email):
        try:
            conn = sqlite3.connect('database.db')
            curssor = conn.cursor()
            query="""select * from user where email = ?"""
            curssor.execute(query,(email,))
            records = curssor.fetchall()
            conn.close()
            if len(records)>0:
              return True
            else:
                False
        except Exception as e:
            print(PrintException())

    def update_user(self, key,isActive):
        try:
            conn = sqlite3.connect('database.db')
            query = """update  user set isActive=? where key =?"""
            conn.execute(query, (isActive,key,))
            conn.commit()
            res = {}
            conn.close()
            return res
        except Exception as e:
            print(PrintException())

    def delete_Users(self):
        try:
            conn = sqlite3.connect('database.db')
            sql = 'DELETE FROM user'
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()
        except Exception as e:
            print(PrintException())