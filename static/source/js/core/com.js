var comver = '07NOV2018@10:10AM'; 

console.log('=======================================');
console.log('== COM VERSION %s             ==',comver);
console.log('=======================================');

var lastUpdate = Math.round((new Date()).valueOf()/1000);
var printScreen = false;  // printScreen = false;
var pingDict    = {};    
var pingList    = [];
function GET_API(func,url,headers)
	{
		try
		{
			var xmlhttp=new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==1 ) { return }     // added this to fix error c00c023f in IE9
					else if (xmlhttp.readyState==4 )    { if(xmlhttp.status==200) { func(xmlhttp.responseText) } }
					else if (xmlhttp.status==500)   	{ func("Server Error Code 500") }
					else if (xmlhttp.status==404) 		{ func("404") 					}
				}
			xmlhttp.open("GET",url,true);							
			xmlhttp.setRequestHeader('apikey', apikey);
			for(myitem in headers) { xmlhttp.setRequestHeader(myitem, headers[myitem]); }
			xmlhttp.send();
		}
		catch(err) { alert("GET() Error: "+err.message) }

	}
function POST_API(func,url,headers,body)
	{
		try
		{
			var xmlhttp=new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.status==404)		{	func("404")	}
					else if (xmlhttp.status==500)	{	func("Server Error Code 500")	}
					else if (xmlhttp.readyState==4 && xmlhttp.status==200) 	{	func(xmlhttp.responseText)	}
				}
			xmlhttp.open('POST',url,true);							
			xmlhttp.setRequestHeader('apikey', apikey);
			//for(myitem in headers) { xmlhttp.setRequestHeader(myitem, headers[myitem]); }
			xmlhttp.setRequestHeader("Content-type", "application/json");
			xmlhttp.send(body);
		}
		catch(err) { alert("POST_API() Error: "+err.message) }
	}	

