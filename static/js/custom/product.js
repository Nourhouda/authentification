$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        margin: 20,
        nav:true,
    });
});

function show_image(id_1, id_2) {
    console.log('id :')
    console.log(id_1)
    console.log('id_2')
    console.log(id_2)
    $(".product-image.active").hide();
    $("product-image.active").removeClass("active");
    //$(".owl-carousel a.active").removeClass("active");
    $(".owl-carousel img.active").removeClass("active");

    $(id_1).show();
    $(id_1).addClass("active");
    //$(id_2).addClass("active");

    $(id_2 + ' img').addClass("active");
}
function next(id){
console.log('id :')
console.log(id)
prev='#hardware-img-'+id
next='#hardware-img-'+id+1
show_image(prev,next)
}
function show_company_section(this_, section_id) {

    $('#company-nav li.active').removeClass('active');
    $(this_).addClass('active');
    $('.company-item.active').hide();
    $(section_id).addClass('active');
    $(section_id).show();
}


/* customer reviews */

function star_hover(i) {

    if(parseInt(i) == 0)
    {
        i = $("#rating-new-value").val();
    }

    for(c=1; c<=5; c++)
    {
        $("form #star-"+c).removeClass("fa-star");
        $("form #star-"+c).addClass("fa-star-o");
    }

    for(c=1; c<=parseInt(i); c++)
    {
        $("form #star-"+c).removeClass("fa-star-o");
        $("form #star-"+c).addClass("fa-star");
    }

}


function star_focus(i) {

    $("#rating-new-value").val(i);

    for(c=1; c<=5; c++)
    {
        $("form #star-"+c).removeClass("fa-star");
        $("form #star-"+c).addClass("fa-star-o");
    }

    for(c=1; c<=parseInt(i); c++)
    {
        $("form #star-"+c).removeClass("fa-star-o");
        $("form #star-"+c).addClass("fa-star");
    }

}


function add_review(event, url) {

    event.preventDefault();

    var rating = parseInt($('#rating-new-value').val());

    var comment = $('#comment').val();

    if(rating == 0 || comment == "")
    {
        return false
    }

    $('.add-review-section .review-loading').text('Loading...');
    $('.add-review-section button').prop('disabled', true);

    $.ajax
    ({
        type: 'POST',
        url: url,
        //contentType: 'application/json;charset=UTF-8',
        data: {'rating-new-value': rating, 'comment': comment},

        success:function(response) {
            console.log(JSON.stringify(response));

            $('.add-review-section .review-loading').text('');
            $('.add-review-section button').prop('disabled', false);

            if(response['valid'] == 1) {

                $('.add-review-section .review-success').text(response['content']);

                setTimeout(function () {
                    $('.add-review-section .review-success').text('');
                }, 5000)
            }
        },
        error: function (error) {
            console.log(JSON.stringify(error));
            $('.add-review-section .review-loading').text('');
            $('.add-review-section button').prop('disabled', false);
        }
    });
}


/* end customer reviews */

function show_product_section(this_, section_id, title = '') {

    $("#product-navigation button.active").removeClass('active');
    $(this_).addClass('active');

    $('#product-mobile-navbar h5').text(title);

    $('.product_section').hide();
    $(section_id).show();
}


function add_cart(url, modal_id)
{
    $(modal_id + ' .loading').text('Adding product to cart...');


    $.ajax
    ({
        type: 'POST',
        url: url,
        //contentType: 'application/json;charset=UTF-8',
        data: {},

        success:function(response) {
            console.log(JSON.stringify(response));

            $(modal_id + ' .loading').text('');

            if(response['valid'] == 1) {

                $(modal_id + ' .success').show();
            }
        },
        error: function (error) {
            console.log(JSON.stringify(error));
            $(modal_id + ' .loading').text('The operation is failed. Please try later.');
        }
    });
}



function add_coupon(url)
{
    $('.coupon-loading').text('Save coupon...');

    $.ajax
    ({
        type: 'POST',
        url: url,
        data: {},

        success:function(response) {
            console.log(JSON.stringify(response));

            $('.coupon-loading').text('');

            if(response['valid'] == 1) {

                $('.coupon-success').show();
            }
        },
        error: function (error) {
            console.log(JSON.stringify(error));
            $('.coupon-loading').text('The operation is failed. Please try later.');
        }
    });
}



