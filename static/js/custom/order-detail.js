$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        margin: 20,
        nav:true,
    });

    $('#summernote').summernote({
        height: 300
    });
});

function show_image(id_1, id_2) {
    $(".product-image.active").hide();
    $("product-image.active").removeClass("active");
    //$(".owl-carousel a.active").removeClass("active");
    $(".owl-carousel img.active").removeClass("active");

    $(id_1).show();
    $(id_1).addClass("active");
    //$(id_2).addClass("active");

    $(id_2 + ' img').addClass("active");
}

function addMessage(event, url) {

    event.preventDefault();

    var content = $('#summernote').val();

    if(content == "")
    {
        return false
    }

    $('form p.loading').text('Loading...');
    $('form button').prop('disabled', true);

    $.ajax
    ({
        type: 'POST',
        url: url,
        //contentType: 'application/json;charset=UTF-8',
        data: {'content': content},

        success:function(response) {
            console.log(JSON.stringify(response));

            $('form p.loading').text('Loading...');
            $('form button').prop('disabled', false);

            if(response['valid'] == 1) {

                $('#order-detail-right').html(response['content'])

                $('#summernote').summernote({
                    height: 300
                });
            }
        },
        error: function (error) {
            console.log(JSON.stringify(error));
            $('form p.loading').text('Loading...');
            $('form button').prop('disabled', true);
        }
    });
}

