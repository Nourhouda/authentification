$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        margin: 20,
        nav:true,
    });

    $('#summernote').summernote({
        height: 300
    });
});

function show_image(id_1, id_2) {
    $(".product-image.active").hide();
    $("product-image.active").removeClass("active");
    //$(".owl-carousel a.active").removeClass("active");
    $(".owl-carousel img.active").removeClass("active");

    $(id_1).show();
    $(id_1).addClass("active");
    //$(id_2).addClass("active");

    $(id_2 + ' img').addClass("active");
}
