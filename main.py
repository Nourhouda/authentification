from flask import Flask
from flask import render_template
from controller.user import login,register,account
from miUtil import *

app = Flask(__name__)
#CORS(app)
app.secret_key = 'somesecretkeytfdhatonmkjinlyishouldknowpo'
@app.route('/')
def index():
   return render_template('index.html')

app.add_url_rule('/login-user',methods=['GET','POST'], view_func=login.login_user)
app.add_url_rule('/register-user',methods=['GET','POST'], view_func=register.register_user)
app.add_url_rule('/send-mail',methods=['GET','POST'], view_func=register.send_mail)
app.add_url_rule('/verify-account',methods=['GET'], view_func=register.View_verify_account)
app.add_url_rule('/activate/account/<userKey>',methods=['GET'], view_func=register.active_account)
app.add_url_rule('/my-account',methods=['GET'], view_func=account.account)
app.add_url_rule('/user-logout',methods=['GET'], view_func=account.logOut)

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html',error=PrintException()),500

@app.errorhandler(404)
def server_error(e):
    return render_template('404.html',error=PrintException()), 404

#Main
if __name__ == '__main__':
 app.run(host='127.0.0.1', port=8085, debug=True)
