import datetime, urllib, json, math, sys, os, time


def toJSON(mydict):
	try:
		return json.dumps(mydict)
	except Exception as e:
		return json.dumps({'error':str(e),'status':-1})


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    #filename = f.f_code.co_filename
    #linecache.checkcache(filename)
    #line = linecache.getline(filename, lineno, f.f_globals)
    #return 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
    return 'EXCEPTION IN ({}, {})'.format(lineno, exc_obj)