from flask import render_template,request,redirect,session
from model.user import *
import uuid
from werkzeug.security import generate_password_hash
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def register_user():
    try:
        success = int(request.args.get('success', -100))
        messageError = ''
        if success == -1:
            messageError = 'this Email is Used please Tap another Email !!'

        if request.method=='POST':
             firstname = request.form['f-name']
             lastname=request.form['l-name']
             email=request.form['email']
             if User().find_user_email(email):
                 return redirect('/register-user?success=-1')
             else:
                 password = generate_password_hash(request.form['password'])
                 isActive=0
                 key = str(uuid.uuid1())
                 key = key.replace('-', '')
                 User().CreateUser(firstname,lastname,email,password,isActive,key)
                 send_mail(key,email)
                 return  redirect('/verify-account')
        else:
           return render_template('register.html',ErrorMessage=messageError)
    except Exception as e:
        print('error')
        print(PrintException())

def send_mail(key,email):
    try:
        sender_email = "******"
        receiver_email = email
        password = '****'

        message = MIMEMultipart("alternative")
        message["Subject"] = "Activate Account"
        message["From"] = sender_email
        message["To"] = receiver_email

        # Create the plain-text and HTML version of your message
        text =''
        html = '<html> <body><p>Hi,<br> Please Click here to Activate your Account<br>'
        html+='<a href="http://127.0.0.1:8085/activate/account/'+key+'">'+'Click here</a></p></body></html>'

        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)

        # Create secure connection with server and send email
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )

        return toJSON({'status':'Ok'})
    except Exception as e:
        return toJSON({'status':'Fail','error':PrintException()})
#py -m smtpd -c DebuggingServer -n localhost:1025
def View_verify_account():
    return render_template('email/verify-Account.html')
def active_account(userKey):
    session.pop('mail', None)
    session.pop('firstname', None)
    session.pop('lastname', None)
    session.pop('authentificated', False)
    user=User().get_user_by_key(userKey)
    if len(user)>0:
        User().update_user(userKey,1)
        session['mail'] = user['email']
        session['firstname'] = user['fistName']
        session['lastname'] = user['lastName']
        session['userKey'] = user['key']
        return render_template('email/account_Active_success.html',userName=user['fistName'])
    else:
        return redirect('/register-user')
