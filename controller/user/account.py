from flask import render_template,request,redirect,session
from model.user import *

def account():
    try:
         return render_template('my-account.html')
    except Exception as e:
        print('error')
        print(PrintException())
def logOut():
    try:
        session.pop('mail', None)
        session.pop('firstname', None)
        session.pop('lastname', None)
        session.pop('authentificated', False)
        session.pop('userKey', None)
        return redirect('/')

    except Exception as e:
        print('error')
        print(PrintException())