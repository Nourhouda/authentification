from miUtil import *
from flask import render_template,request,redirect,session
from model.user import *
from werkzeug.security import generate_password_hash, check_password_hash

def login_user():
    try:
         success = int(request.args.get('success', -100))
         messageError = ''
         if success == -1:
             messageError = 'We cannot find an account with that email address'
         if success == -2:
             messageError = 'Please verify your password'
         if success == -3:
             messageError = 'Please check your Email to activate your Account'
             session.pop('mail', None)
             session.pop('firstname', None)
             session.pop('lastname', None)
             session.pop('authentificated', False)
             session.pop('userKey', None)
         if request.method == 'POST':
             email = request.form['email']
             password=request.form['password']
             if User().find_user_email(email):
                 user=User().get_user_by_email(email)
                 if not check_password_hash(user['password'], password) :
                     return redirect('/login-user?success=-2')
                 if user['isActive']==0:
                     return redirect('/login-user?success=-3')
                 if check_password_hash(user['password'], password) and user['isActive']==1:
                     session['mail'] = user['email']
                     session['firstname'] = user['fistName']
                     session['lastname'] = user['lastName']
                     session['userKey'] = user['key']
                     return redirect('/my-account')
             else:
                 return redirect('/login-user?success=-1')

         else:
             return render_template('login.html',ErrorMessage=messageError)
    except Exception as e:
        print('error')
        print(PrintException())
